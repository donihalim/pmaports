# Maintainer: Adam Thiede <me@adamthiede.com>
# Co-Maintainer: Jenneron <jenneron@protonmail.com>
pkgname=linux-postmarketos-mediatek-mt8173
pkgver=6.7.6
pkgrel=1
pkgdesc="Mainline kernel fork for Mediatek MT8173 devices"
arch="aarch64"
_carch="arm64"
_flavor="${pkgname#linux-}"
url="https://kernel.org"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-community
"
makedepends="
	bash
	bison
	findutils
	flex
	installkernel
	openssl-dev
	perl
	gmp-dev
	mpc1-dev
	mpfr-dev
	xz
"

# Source
_config="config-$_flavor.$arch"
case $pkgver in
	*.*.*)	_kernver=${pkgver%.0};;
	*.*)	_kernver=$pkgver;;
esac
source="
	https://cdn.kernel.org/pub/linux/kernel/v6.x/linux-${pkgver//_/-}.tar.xz
	$_config
	fix-mmc1-speed.patch
	fix-mmc-order.patch
	fix-spi-nor-max-frequency.patch
"
builddir="$srcdir/linux-${_kernver//_/-}"

prepare() {
	default_prepare
	cp "$srcdir/config-$_flavor.$CARCH" .config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-postmarketOS"
}

package() {
	mkdir -p "$pkgdir"/boot
	make zinstall modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_PATH="$pkgdir"/boot \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir/boot/dtbs"
	rm -f "$pkgdir"/lib/modules/*/build "$pkgdir"/lib/modules/*/source

	install -D "$builddir"/include/config/kernel.release \
		"$pkgdir"/usr/share/kernel/$_flavor/kernel.release
}

sha512sums="
a8f40ece7438178a9ce70f82cff1184d93d304bef347fab175b94ed714d4b7541e8c49ecaaa4169a66e18b33e8aeaad2e8753b9632cff2035adab6b9bbb4d392  linux-6.7.6.tar.xz
201f5db6494eeff0a4a28f6c3df42b2f261abc5a1d04fae344912b7fb0adfa18ebb25d09bbdd6ce19f76046e7be636160842ff25c9a89e22efd4a07ef9be4082  config-postmarketos-mediatek-mt8173.aarch64
4b499c1fbf53631cffd6fa7299643dc744e0e2187f71804664b02f05296162b42e3f76aa0d8c688cecb43a8bcd41ec92991c98287951292260237b828dcca710  fix-mmc1-speed.patch
c86f8dfc32165a32381d45a4c8b6811ebf43f01b5d8a48fbe227cf5084cfefe24b32264c1c150cb49115db4759a2d21ad48b37dcaac78367d226e9cc2a5ba849  fix-mmc-order.patch
caf48ac0f77661153ee94c7de4931baca135a69a97e93f01ad8f276b4a9944e077d7214c117450943cca07990c773661f79718cb0c2ff7c5789c93d37afb26de  fix-spi-nor-max-frequency.patch
"
